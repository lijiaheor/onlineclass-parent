package com.ljh.media.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.media.model.po.MqMessageHistory;
public interface MqMessageHistoryMapper extends BaseMapper<MqMessageHistory> {

}
