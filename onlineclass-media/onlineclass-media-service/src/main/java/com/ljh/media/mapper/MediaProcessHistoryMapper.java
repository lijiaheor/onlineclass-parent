package com.ljh.media.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.media.model.po.MediaProcessHistory;

public interface MediaProcessHistoryMapper extends BaseMapper<MediaProcessHistory> {

}
