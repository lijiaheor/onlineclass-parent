package com.ljh.media.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.media.model.po.MqMessage;


public interface MqMessageMapper extends BaseMapper<MqMessage> {

}
