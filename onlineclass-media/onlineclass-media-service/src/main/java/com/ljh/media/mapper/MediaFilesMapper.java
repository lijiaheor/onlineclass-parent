package com.ljh.media.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.media.model.po.MediaFiles;

/**
 * 媒资信息 Mapper 接口
 */
public interface MediaFilesMapper extends BaseMapper<MediaFiles> {

}
