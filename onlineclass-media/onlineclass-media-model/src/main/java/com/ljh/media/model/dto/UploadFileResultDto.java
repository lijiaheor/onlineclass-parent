package com.ljh.media.model.dto;

import com.ljh.media.model.po.MediaFiles;
import lombok.Data;
import lombok.ToString;

/**
 * @description 上传普通文件成功响应结果
 */
@Data
public class UploadFileResultDto extends MediaFiles {

}