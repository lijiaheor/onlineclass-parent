package com.ljh.base.exception;
/**
 * @description 本项目自定义异常类型
 */
public class OnlineClassException extends RuntimeException {

    private String errMessage;

    public OnlineClassException() {
    }

    public OnlineClassException(String message) {
        super(message);
        this.errMessage = message;

    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public static void cast(String message){
        throw new OnlineClassException(message);
    }
    public static void cast(CommonError error){
        throw new OnlineClassException(error.getErrMessage());
    }

}
