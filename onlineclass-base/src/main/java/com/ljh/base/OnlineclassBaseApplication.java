package com.ljh.base;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class OnlineclassBaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnlineclassBaseApplication.class, args);
    }
}
