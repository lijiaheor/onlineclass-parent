package com.ljh.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.system.model.po.Dictionary;

/**
 * 数据字典 Mapper 接口
 * @author liusp
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}
