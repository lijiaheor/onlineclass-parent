package com.ljh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineclassContentApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassContentApplication.class, args);
    }

}
