package com.ljh.content.api.controller;
import com.ljh.content.model.dto.TeachplanDto;
import com.ljh.content.model.po.CourseTeacher;
import com.ljh.content.service.service.CourseCategoryService;
import com.ljh.content.service.service.CourseTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description 师资管理
 */
@Api(value = "师资管理接口",tags = "师资管理接口")
@RestController
public class CourseTeacherController {
    @Autowired
    CourseTeacherService courseTeacherService;

    @ApiOperation("查询教师")
    @GetMapping("/courseTeacher/list/{courseId}")
    public List<CourseTeacher> getCourseTeacher(@PathVariable Long courseId){
        System.out.println(courseId);
        return courseTeacherService.getCourseTeacher(courseId);
    }
    @ApiOperation("新增/修改课程教师")
    @PostMapping("/courseTeacher")
    public CourseTeacher createCourseTeacher(@RequestBody CourseTeacher courseTeacher){
        //机构id，由于认证系统没有上线暂时硬编码
        return courseTeacherService.createCourseTeacher(courseTeacher);
    }
    /**
     * 根据课程ID和
     * @param courseId
     * @param id
     */
    @ApiOperation("删除教师")
    @DeleteMapping("/courseTeacher/course/{courseId}/{id}")
    public void deleteCourseTeacher(@PathVariable Long courseId ,@PathVariable Long id){
        courseTeacherService.deleteCourseTeacher(courseId,id);
    }
}