package com.ljh.content.api;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableSwagger2Doc
@ComponentScan(value="com.ljh")
public class OnlineclassContentApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassContentApiApplication.class, args);
    }

}
