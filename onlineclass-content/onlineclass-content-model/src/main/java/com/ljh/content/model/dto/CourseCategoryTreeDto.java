package com.ljh.content.model.dto;

import com.ljh.content.model.po.CourseCategory;
import lombok.Data;

import java.util.List;

/**
 * @description 课程分类树型结点dto
 */
@Data
public class CourseCategoryTreeDto extends CourseCategory implements java.io.Serializable {

   //子节点
   List<CourseCategoryTreeDto> childrenTreeNodes;

}
