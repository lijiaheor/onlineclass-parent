package com.ljh.content.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineclassContentModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassContentModelApplication.class, args);
    }

}
