package com.ljh.content.service.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.content.model.dto.CourseCategoryTreeDto;
import com.ljh.content.model.po.CourseCategory;
import java.util.List;
/**
 * 课程分类 Mapper 接口
 */
public interface CourseCategoryMapper extends BaseMapper<CourseCategory> {
    //使用递归查询分类
    public List<CourseCategoryTreeDto> selectTreeNodes(String id);
}
