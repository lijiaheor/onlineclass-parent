package com.ljh.content.service.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.content.model.po.TeachplanMedia;
/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface TeachplanMediaMapper extends BaseMapper<TeachplanMedia> {

}
