package com.ljh.content.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.content.model.po.CourseTeacher;

/**
 * <p>
 * 课程-教师关系表 Mapper 接口
 * </p>
 */
public interface CourseTeacherMapper extends BaseMapper<CourseTeacher> {

}
