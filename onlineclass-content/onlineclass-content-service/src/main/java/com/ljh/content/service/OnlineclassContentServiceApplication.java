package com.ljh.content.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineclassContentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassContentServiceApplication.class, args);
    }

}
