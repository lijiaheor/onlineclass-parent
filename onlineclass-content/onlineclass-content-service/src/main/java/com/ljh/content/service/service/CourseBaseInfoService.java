package com.ljh.content.service.service;

import com.ljh.base.model.PageParams;
import com.ljh.base.model.PageResult;
import com.ljh.content.model.dto.AddCourseDto;
import com.ljh.content.model.dto.CourseBaseInfoDto;
import com.ljh.content.model.dto.QueryCourseParamsDto;
import com.ljh.content.model.po.CourseBase;
/**
 * @description 课程基本信息管理业务接口
 */
public interface CourseBaseInfoService {
    /*
     * @description 课程查询接口
     * @param pageParams 分页参数
     * @param queryCourseParamsDto 条件条件
     */
    PageResult<CourseBase> queryCourseBaseList(PageParams pageParams, QueryCourseParamsDto queryCourseParamsDto);
    /**
     * 新增课程
     * @param companyId 机构id
     * @param addCourseDto 课程信息
     * @return 课程详细信息
     */
    public CourseBaseInfoDto createCourseBase(Long companyId, AddCourseDto addCourseDto);
}