package com.ljh.content.service.service.impl;
import com.ljh.content.model.po.CourseTeacher;
import com.ljh.content.service.mapper.CourseTeacherMapper;
import com.ljh.content.service.service.CourseTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @description 师资管理业务层接口实现类
 */
@Service
public class CourseTeacherServiceImpl implements CourseTeacherService {
    @Autowired
    CourseTeacherMapper courseTeacherMapper;

    @Override
    public List<CourseTeacher> getCourseTeacher(Long courseId) {
        Map map=new HashMap();
        map.put("course_id",courseId);
        return courseTeacherMapper.selectByMap(map);
    }

    public CourseTeacher createCourseTeacher(CourseTeacher courseTeacher){
        System.out.println("====================="+courseTeacher.toString());
        if (courseTeacher.getId() == null){
            courseTeacherMapper.insert(courseTeacher);
        }else{
            courseTeacherMapper.updateById(courseTeacher);
        }
        return courseTeacher;
    }
    @Override
    public void deleteCourseTeacher(Long courseId, Long id) {
        Map map=new HashMap();
        map.put("id",id);
        map.put("course_id",courseId);
        courseTeacherMapper.deleteByMap(map);
    }
}
