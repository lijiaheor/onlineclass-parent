package com.ljh.content.service.service;
import com.ljh.content.model.po.CourseTeacher;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
/**
 * @description 师资管理业务层接口
 */
public interface CourseTeacherService {
    /**
     * 通过课程ID查询教师信息
     * @param courseId
     * @return
     */
    public List<CourseTeacher> getCourseTeacher(Long courseId);
    /**
     * 添加/修改课程教师信息
     * @param courseTeacher
     * @return
     */
    public CourseTeacher createCourseTeacher(CourseTeacher courseTeacher);
    /**
     * 通过课程ID 和教师ID删除课程授课教师信息
     * @param courseId
     * @param id
     */
    public void deleteCourseTeacher(Long courseId,Long id);

}