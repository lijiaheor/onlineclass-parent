package com.ljh.content.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljh.content.model.po.CoursePublishPre;

/**
 * <p>
 * 课程发布 Mapper 接口
 * </p>
 */
public interface CoursePublishPreMapper extends BaseMapper<CoursePublishPre> {

}
