package com.ljh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineclassGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassGatewayApplication.class, args);
    }

}
